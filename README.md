This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

# RavenPack assignment
Test project for The RavenPack JavaScript vacancy.

tl;dr; Production mode in Firebase [link](https://rp-abc-soup.firebaseapp.com).

## Development Prerequisites

Before you start installing things, make sure that you have:

```
  "node": ">=8.9.4",
  "npm": ">=5.6.0",
  "yarn": ">=1.6.0"
```

## Running Project

Install dependencies
```bash
$ yarn
```
Run project in development mode
```bash
$ yarn dev
```

## Running Staging and Production builds

Install serve globally
```bash
$ yarn global add serve
```
Run project in development mode
```bash
$ yarn build-stage # or build-prod
$ serve -s build
```

## Firebase

### To host project on firebase system you need to do following

Install firebase-tools
```bash
$ npm install -g firebase-tools
```
or
```bash
$ yarn global add firebase-tools
```

Get invitation to the project.
Once you've been invited accept invite and login via your console to firebase
```bash
$ firebase login # It should take you to the browser automatically
```

If all steps before this one were success only thing left to do is to call this command 
```bash
$ yarn deploy
```
