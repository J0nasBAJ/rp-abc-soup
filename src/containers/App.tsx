import * as React from 'react';
import Form from '../components/Form';
import styled, { createGlobalStyle, GlobalStyleClass } from 'styled-components';
import { getUserId } from '../helpers';
import { db } from '../database';
import VersionBox from '../components/VersionBox';

const GlobalStyle: GlobalStyleClass<{}, {}> = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
    min-width: 555px;
    -moz-osx-font-smoothing: grayscale;
    -webkit-font-smoothing: antialiased;
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen",
      "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",
      sans-serif;
  }
  
  code {
    font-family: source-code-pro, Menlo, Monaco, Consolas, "Courier New",
      monospace;
  }
  
  textarea {
    resize: none;
    padding: 5px;
  }
  
  h3, h4 {
    margin: 1vh 0;
  }
`;

const Container = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: start;
`;

class App extends React.Component {

  public componentDidMount() {
    this.logUser();
  }

  public render() {
    return (
      <Container>
        <GlobalStyle/>
        <VersionBox/>
        <h1>Welcome to Alphabet Soup</h1>
        <span style={{ textAlign: 'center', opacity: 0.7 }}>
          Everyone loves alphabet soup. And of course, <br/>
          you want to know if you can construct a message <br/>
          from the letters found in your bowl.
        </span>
        <Form/>
      </Container>
    );
  }

  private logUser = async () => {
    const userId = getUserId();
    db.ref(`activity/${userId}`).set(Date());
  }
}

export default App;
