import { config } from './config';
import { database, initializeApp } from 'firebase';

if (!config.FIREBASE) {
  throw new Error('Error: database config not provided');
}

const defaultDatabase = initializeApp(config.FIREBASE);
const db: database.Database = defaultDatabase.database();
export { db };
