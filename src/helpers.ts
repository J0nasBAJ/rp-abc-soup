import uuid from 'uuid';
import { IMessageCompositionInfo, IMessageStringInfo } from './types/global';

const userIdKey = '@gigAddressBookUserId';
export const sampleMessage: string = `The fast brown fox jumps over the lazy dog`;
export const sampleSoup: string = `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
`;

const makeMessageCharHashMap = (message: string): IMessageStringInfo => {
  const total: number = message.length;
  const charMap: { [char: string]: number } = {};

  let iterations: number = Math.floor(total / 2);
  const isMessageArrayEven = total % 2 === 0;
  if (!isMessageArrayEven) {
    iterations++;
  }

  for (let i = 0; i < iterations; i++) {
    const charFront: string = message.charAt(i);
    const charBack: string = message.charAt(total - 1 - i);

    if (i !== total - 1 - i) {
      charMap[charFront] = charMap[charFront] ? charMap[charFront] + 1 : 1;
      charMap[charBack] = charMap[charBack] ? charMap[charBack] + 1 : 1;
    } else {
      charMap[charFront] = charMap[charFront] ? charMap[charFront] + 1 : 1;
    }
  }
  return { charMap, total };
};

export const soupContainsRequiredLetters = (
  charSoup: string,
  message: string
): IMessageCompositionInfo => {
  const total = charSoup.length;
  const messageInfo = makeMessageCharHashMap(message);

  let actualIterations: number = 0;
  let iterations: number = Math.floor(total / 2);
  const isSoupArrayEven = total % 2 === 0;
  if (!isSoupArrayEven) {
    iterations++;
  }

  for (let i = 0; i < iterations; i++) {
    const charFront: string = charSoup.charAt(i);
    const charBack: string = charSoup.charAt(total - 1 - i);

    if (i !== total - 1 - i) {
      if (messageInfo.charMap[charFront]) {
        messageInfo.charMap[charFront] -= 1;
        messageInfo.total -= 1;
      }
      if (messageInfo.charMap[charBack]) {
        messageInfo.charMap[charBack] -= 1;
        messageInfo.total -= 1;
      }
    } else if (messageInfo.charMap[charFront]) {
      messageInfo.charMap[charFront] -= 1;
      messageInfo.total -= 1;
    }

    actualIterations++;
    if (messageInfo.total === 0) {
      break;
    }
  }

  return {
    iterations: actualIterations,
    availableToCompose: messageInfo.total === 0,
  };
};

const makeMessageCharHashMapOld = (message: string): IMessageStringInfo => {
  const total: number = message.length;
  const charMap: { [char: string]: number } = {};
  for (let i = 0; i < total; i++) {
    const char: string = message.charAt(i);
    charMap[char] = charMap[char] ? charMap[char] + 1 : 1;
  }
  return { charMap, total };
};

export const soupContainsRequiredLettersOld = (charSoup: string, message: string) => {
  const messageInfo = makeMessageCharHashMapOld(message);

  for (let i = 0; i < charSoup.length; i++) {
    const soupChar: string = charSoup.charAt(i);
    if (messageInfo.charMap[soupChar]) {
      messageInfo.charMap[soupChar] -= 1;
      messageInfo.total -= 1;
    }

    if (messageInfo.total === 0) {
      return true;
    }
  }

  return false;
};

export const getUserId = () => {
  const rawState = localStorage.getItem(userIdKey);
  const userId: string = rawState ? rawState : uuid.v1();
  localStorage.setItem(userIdKey, userId);
  return userId;
};
