import { soupContainsRequiredLetters } from '../helpers';

describe('src/helpers', () => {
  const soup: string = '12345';
  const message: string = '12345';

  const soup1: string = 'Abcdefgh';
  const message1: string = '12345';

  const soup2: string = 'ABCDE';
  const message2: string = 'edcba';

  it(`Should return true when message: "${message}" and soup: "${soup}"`, () => {
    const { availableToCompose } = soupContainsRequiredLetters(soup, message);
    expect(availableToCompose).toBeTruthy();
  });

  it(`Should return false when message: "${message1}" and soup: "${soup1}"`, () => {
    const { availableToCompose } = soupContainsRequiredLetters(soup1, message1);
    expect(availableToCompose).toBeFalsy();
  });

  it(`Should return false when message: "${message2}" and soup: "${soup2}"`, () => {
    const { availableToCompose } = soupContainsRequiredLetters(soup2, message2);
    expect(availableToCompose).toBeFalsy();
  });

  it(`Should return true when message: "${message2}" and soup: "${soup2}" is in lowercase`, () => {
    const { availableToCompose } = soupContainsRequiredLetters(
      soup2.toLowerCase(),
      message2.toLowerCase(),
    );
    expect(availableToCompose).toBeTruthy();
  });

  it('Should return true when message and soup is empty strings', () => {
    const { availableToCompose } = soupContainsRequiredLetters('', '');
    expect(availableToCompose).toBeTruthy();
  });
});
