import * as React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import BigO from '../../components/BigO';

describe('<Counter />', () => {
  it('renders', () => {
    const renderer = createRenderer();

    expect(renderer.render(
      <BigO
        messageLength={5}
        soupIterations={3}
        totalIterations={8}
      />
    )).toMatchSnapshot();
  });
});
