import * as React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  width: 80%;
  display: flex;
  margin: 3vh 0;
  align-items: start;
  flex-direction: column;
  justify-content: center;
`;

interface IBigOProps {
  messageLength: number;
  soupIterations: number;
  totalIterations: number;
}

const BigO: React.SFC<IBigOProps> = (props: IBigOProps) => {
  return (
    <Container>
      <h3>Big-O calculations summary:</h3>
      <h4>Worst case scenario:</h4>
      <code style={{ marginLeft: '4vw' }}>
        O(n) = O(Message.length/2 + 1) + O(Soup.length/2 + 1)
      </code>
      <h4>Your scenario (total iterations):</h4>
      <code style={{ marginLeft: '4vw' }}>
        {`O(${props.totalIterations}) = O(${props.messageLength}) + O(${props.soupIterations})`}
      </code>
    </Container>
  );
};

export default BigO;
