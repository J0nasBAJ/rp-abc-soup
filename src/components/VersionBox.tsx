import * as React from 'react';
import { config } from '../config';

const VersionBox: React.SFC<{}> = () => (
  <div style={{ position: 'fixed', top: 0, right: 0, margin: '10px' }}>
    <span>ENV: {config.REACT_APP_ENV}</span>
    <br/>
    <span>Version: {config.VERSION}</span>
  </div>
);

export default VersionBox;
