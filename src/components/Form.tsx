import * as React from 'react';

import BigO from './BigO';
import styled from 'styled-components';
import DefaultButton from '../components/DefaultButton';
import DefaultTextArea from '../components/DefaultTextArea';

import { debounce } from 'lodash';
import { TextChangeEvent } from '../types/global';
import { sampleMessage, sampleSoup, soupContainsRequiredLetters } from '../helpers';

interface IContainer {
  m?: string; // margin
}

const Container = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin: ${(p: IContainer) => p.m || '3vh 0'};
`;

interface IState {
  message: string;
  charSoup: string;
  soupIterations: number;
}

export default class Form extends React.Component<{}, IState> {
  public state = {
    message: '',
    charSoup: '',
    soupIterations: 0,
  };

  private onCheckPressed = debounce(() => {
    const { message, charSoup } = this.state;

    if (message && charSoup) {
      const messageCouldBeComposed = soupContainsRequiredLetters(
        charSoup.toLowerCase(),
        message.toLowerCase(),
      );

      this.setState({
        soupIterations: messageCouldBeComposed.iterations,
      });
      if (messageCouldBeComposed.availableToCompose) {
        alert('Message could be constructed');
      } else {
        alert('Message could not be constructed');
      }
    } else {
      alert('Please provide message and/or soup');
    }
  }, 300);

  public render() {
    const { charSoup, message, soupIterations } = this.state;
    const messageIterations: number = message.length
      ? Math.floor(message.length / 2) + 1
      : 0;
    const totalIterations: number = messageIterations + soupIterations;

    return (
      <>
        <Container>
          <DefaultTextArea
            value={message}
            draggable={false}
            placeholder="Message"
            onChange={this.updateTextState('message')}
          />
          <DefaultTextArea
            value={charSoup}
            draggable={false}
            placeholder="Soup"
            onChange={this.updateTextState('charSoup')}
          />
        </Container>
        <Container m="0">
          <DefaultButton
            m="0 10px"
            onClick={this.onCheckPressed}
          >
            Can I compose it?!
          </DefaultButton>
          <DefaultButton
            m="0 10px"
            onClick={this.setSampleText}
          >
            Set example strings
          </DefaultButton>
        </Container>
        <BigO
          soupIterations={soupIterations}
          totalIterations={totalIterations}
          messageLength={messageIterations}
        />
      </>
    );
  }

  private updateTextState = (stateField: keyof IState) => (e: TextChangeEvent) => {
    const value: string = e.target ? e.target.value : '';
    this.setState((prevState: IState) => ({
      ...prevState,
      soupIterations: 0,
      [stateField]: value,
    }));
  };

  private setSampleText = () => this.setState({
    soupIterations: 0,
    charSoup: sampleSoup,
    message: sampleMessage,
  });
}
