import React from 'react';
import styled from 'styled-components';

interface IDefaultButton extends React.HTMLProps<HTMLInputElement> {
  m?: string;
  p?: string;
  b?: string;
  br?: string;
  transition?: string;
  transform?: 'uppercase' | 'lowercase' | 'capitalize' | 'none';
}

const DefaultButton = styled.button`
  outline: none;
  color: #FFFFFF;
  font-size: 14px;
	font-weight:bold;
  font-weight: 600;
  line-height: 1.2em;
  display: inline-block;
  text-decoration: none;
  background-color:#768d87;
  -moz-border-radius:5px;
	-webkit-border-radius:5px;

  margin: ${(p: IDefaultButton) => p.m || 'none'};
  padding: ${(p: IDefaultButton) => p.p || '10px'};
  border-radius: ${(p: IDefaultButton) => p.br || '3px'};
  border: ${(p: IDefaultButton) => p.b || '1px solid #323330'};
  text-transform: ${(p: IDefaultButton) => p.transform || 'none'};
  transition: ${(p: IDefaultButton) =>
  p.transition || 'background .3s, color .3s'};

  &:enabled:hover {
    svg {
      fill: #f5da55;
    }
    color: #f5da55;
    background-color: #323330;
  }

  &:disabled {
    opacity: 0.5;
  }

  &:active {
    opacity: 0.5;
  }
`;

export default DefaultButton;
