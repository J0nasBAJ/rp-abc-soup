import styled from 'styled-components';

const DefaultInput = styled.input`
  height: 40px;
  width: 250px;
  font-size: 18px;
  border-radius: 5px;
  padding-left: 10px;
  margin-right: 10px;
  border: 1px solid grey;
  
  &:focus {
    outline: none;
    background-color: #ececec;
  }

  &:hover {
    cursor: pointer;
  }
`;

export default DefaultInput;
