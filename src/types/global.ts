import { ChangeEvent } from 'react';

export type IEnv = 'development' | 'production' | 'staging' | '';

export interface IConfig {
  NODE_ENV: IEnv;
  VERSION: string;
  REACT_APP_ENV: IEnv;
  PORT: number | string;
  FIREBASE: {
    apiKey?: string;
    authDomain?: string;
    databaseURL?: string;
    projectId?: string;
    storageBucket?: string;
    messagingSenderId?: string;
  };
}

export interface IMessageStringInfo {
  total: number;
  charMap: { [char: string]: number };
}

export interface IMessageCompositionInfo {
  iterations: number;
  availableToCompose: boolean;
}

export type TextChangeEvent = ChangeEvent<HTMLInputElement | HTMLTextAreaElement>;
